import { combineReducers } from "redux";
import authReducer from './authReducer';
import DalalbullReducer from "../DalalbullReducer/DalalbullReducer";
import KryptosReducer from "../KryptosReducer/KryptosReducer";

const rootReducer = combineReducers({
    auth: authReducer,
    Dalalbull: DalalbullReducer,
    Kryptos: KryptosReducer,
});

export type rootType = ReturnType<typeof rootReducer>;

export default rootReducer;