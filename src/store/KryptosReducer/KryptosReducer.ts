import {stateType} from "./Constants";

let initialState: stateType = {
    ranklist: [],
    level: 1,
    rank: 0,
    imgUrl: '',
    sourceHint: '',
    hintText: ['Stay tuned for hints!'],
};

const reducer = (state = initialState, action: any): stateType => {
    switch (action.type) {
        case 'SET_RANK_LIST':
            return {
                ...state,
                ranklist: action.payload,
            };
        case 'SET_LEVEL':
            return {
                ...state,
                level: action.payload,
            };
        case 'SET_RANK':
            return {
                ...state,
                rank: action.payload,
            };
        case 'SET_IMG_URL':
            return {
                ...state,
                imgUrl: action.payload,
            };
        case 'SET_SOURCE_HINT':
            return {
                ...state,
                sourceHint: action.payload,
            };
        case 'SET_HINT_TEXT':
            return {
                ...state,
                hintText: action.payload,
            };    
        default:
            return state;
    }
}

export default reducer;