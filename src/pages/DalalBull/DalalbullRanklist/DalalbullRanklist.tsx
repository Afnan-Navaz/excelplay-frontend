import React, { useState, useEffect, FunctionComponent } from 'react';
import { getRanklist } from '../DalalbullComponents/apicalls/apicalls';
import './DalalbullRanklist.scss';

type Props = {
  name : string,
  pic : string,
  rank : number,
  level : any
}

const Rank : FunctionComponent<Props> = ({ name, pic, rank, level } : Props) => {
  return (
    <tr>
      <td>{rank}</td>
      <td>
        <img src={pic} alt="" className="propic" />
        {name}
      </td>
      <td>{level}</td>
    </tr>
  );
};

const DalalbullRanklist = () => {
  const [ranklist, setRanklist] = useState([]);
  const data : any =[{name: "person1", pic:"picture", rank:1,level:50000},{name: "person2", pic:"picture", rank:2,level:25000},{name: "person3", pic:"picture", rank:3,level:10000}];
  useEffect(() => {
    /*getRanklist().then(res => setRanklist(res.ranklist));*/
    setRanklist(data);
  }, []);
  return (
    <div className="content ranklist">
      <h2>Ranklist</h2>
      <table className="table">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Net Worth</th>
          </tr>
        </thead>
        <tbody>
          {ranklist.map((rank, i) => (
            <Rank {...rank} key={i} />
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default DalalbullRanklist;
